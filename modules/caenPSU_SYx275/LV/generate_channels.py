# This Python script is attached to a display
# and triggered by loc://initial_trigger2$(DID)(1)
# to execute once when the display is loaded.
# TODO: read nbrOfChannels from config or PV

import os, re
from org.csstudio.display.builder.runtime.script import ScriptUtil

# Find file relative to display & read configuration from file
display_file = ScriptUtil.workspacePathToSysPath(widget.getDisplayModel().getUserData("_input_file"))
directory = os.path.dirname(display_file)
parent_directory = re.sub('/\wV', '', directory)
file = parent_directory + "/configuration.txt"
with open(file) as f:
    lines = f.readlines()
display = widget.getDisplayModel()
old_macros = display.getEffectiveMacros()
card_nr = old_macros.getValue('SLOT')
nbrOfchannels = int(lines[int(card_nr)].split(":")[2])

channels = []
for i in range(nbrOfchannels):
    channels.append({
                    'NAME' : "Channel %d" % (i),
                    'CHAN' : "00%d" % (i) if (i) < 10 else "0%d" % (i)
                    })

# Create display:
# For each 'channel', add one embedded display
# which then links to the single_channel.bob
# with the macros of the device.
from org.csstudio.display.builder.model import WidgetFactory

embedded_width = 760
embedded_height = 20

def createInstance(x, y, macros):
    embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
    embedded.setPropertyValue("x", x)
    embedded.setPropertyValue("y", y)
    embedded.setPropertyValue("width", embedded_width)
    embedded.setPropertyValue("height", embedded_height)
    embedded.setPropertyValue("resize", "2")
    for macro, value in macros.items():
        embedded.getPropertyValue("macros").add(macro, value)
    embedded.setPropertyValue("file", "single_channel.bob")
    return embedded

display = widget.getDisplayModel()
# coordinates of where the headers stop
startX = 20
startY = 110
# resolution of display
resX = 800
resY = 600
for i in range(len(channels)):
    x = startX
    y = startY + (embedded_height * (i))
    instance = createInstance(x, y, channels[i])
    display.runtimeChildren().addChild(instance)
