# In 'configuration.txt':
# Number of rows determines how many slots on crate, can leave as 16
# For each row it is defined which card is inserted (type, channels)

import os
from org.csstudio.display.builder.runtime.script import ScriptUtil

# Find file relative to display & read configuration from file
display_file = ScriptUtil.workspacePathToSysPath(widget.getDisplayModel().getUserData("_input_file"))
directory = os.path.dirname(display_file)
file = directory + "/configuration.txt"
with open(file) as f:
    lines = f.readlines()

# Determine card type
hvCards = []
lvCards = []
for line in lines:
    if line.split(":")[1] == 'hv':
        hvCards.append(int(line.split(":")[0]))
    elif line.split(":")[1] == 'lv':
        lvCards.append(int(line.split(":")[0]))

nbrOfCards = len(lines)
cards = []
for i in range(nbrOfCards):
    cards.append({
                 'NAME' : "BOARD %d" % (i),
                 'SLOT' : "0%d" % (i) if (i) < 10 else "%d" % (i)
                 })

from org.csstudio.display.builder.model import WidgetFactory

# Define widget properties
def createInstance(x, y, id, macros):
    embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
    embedded.setPropertyValue("x", x)
    embedded.setPropertyValue("y", y)
    embedded.setPropertyValue("width", embedded_width)
    embedded.setPropertyValue("height", embedded_height)
    embedded.setPropertyValue("resize", "2")
    for macro, value in macros.items():
        embedded.getPropertyValue("macros").add(macro, value)
    if id in hvCards:
        embedded.setPropertyValue("file", "card_hv.bob")
    elif id in lvCards:
        embedded.setPropertyValue("file", "card_lv.bob")
    else:
        embedded.setPropertyValue("file", "card_none.bob")
    return embedded

# Create widgets
embedded_width = 70
embedded_height = 400
gap = 5
startX = 45
startY = 360
display = widget.getDisplayModel()
for i in range(len(cards)):
    x = startX + ( (i) * embedded_width + (i) * gap )
    y = startY
    instance = createInstance(x, y, (i), cards[i])
    display.runtimeChildren().addChild(instance)
