# dg_epics_caen_gui

## GUI for CAEN SYx527 power supplies

Load project/folder in (ESS') CSS Display Builder, main.bob is the file to launch from

0. Turn on and set up the crate itself (network and prefix)
1. Modify macro $DEV in main.bob to match your prefix
2. Specify your crate configuration in configuration.txt (slot:type_of_card:number_of_channels, so a high voltage card with 48 channels in slot 3 is 3:hv:48)

## Good to know

This GUI is only to enable us to start using the 3 CAEN SY4527 crates that we (DG) have, and is in no shape or form the final version. Some well-needed features (e.g. grouping channels) have been ignored since they probably should be sorted out by a Soft IOC rather than in CSS-DB.

### Good features to add

* Auto-detection of plugged in boards (type and nr of channels)
* Being able to switch between boards from the HV/LV-controls page
* Being able to switch between boards AND channels from the Advanced Channel Control pages
